<?php

/**
 * @file
 * Rules module integration for the rules_run_once module.
 */

/**
 * Implements hook_rules_condition_info().
 */
function rules_run_once_rules_condition_info() {
  $conditions = array();

  // Return FALSE if a rule with a particular identifier has not run yet.
  $conditions['rules_run_once_has_not_run_yet'] = array(
    'group' => t('Rules: run once'),
    'label' => t('Rule has not run yet'),
    'parameter' => array(
      'identifier' => array(
        'type' => 'text',
        'label' => t('Identifier'),
        'description' => t('A string used for identifying this rule. Usually the rule name plus any tokens to uniquely identify the thing that the rule is running on.'),
        'optional' => FALSE,
      ),
    ),
  );

  return $conditions;
}

/**
 * Implements hook_rules_action_info().
 */
function rules_run_once_rules_action_info() {
  $actions = array();

  // Record that a rule with a particular identifer has run.
  $actions['rules_run_once_record_run'] = array(
    'group' => t('Rules: run once'),
    'label' => t('Record that rule ran'),
    'parameter' => array(
      'identifier' => array(
        'type' => 'text',
        'label' => t('Identifier'),
        'description' => t('A string used for identifying this rule. Usually the rule name plus any tokens to uniquely identify the thing that the rule is running on.'),
        'optional' => FALSE,
      ),
    ),
    'provides' => array(),
  );

  return $actions;
}

/**
 * Return FALSE if a rule with a particular identifier has not run yet.
 *
 * @param string $identifier
 *   A string used for identifying this rule.
 *
 * @return bool
 *   Returns FALSE if a rule with a particular identifier has not run yet, TRUE
 *   otherwise.
 */
function rules_run_once_has_not_run_yet($identifier) {
  $answer = TRUE;

  // Check if the identifier exists in the database.
  $exists_query = db_select('rules_run_once')
    ->condition('identifier', $identifier, '=')
    ->countQuery();
  $result = $exists_query->execute()->fetchField();

  // If the identifier does exist in the databse, then the rule has already run,
  // so return FALSE so that this rule is not run again for this identifier.
  if ($result > 0) {
    $answer = FALSE;
  }
  // If the identifier does not exist, then the rule has not run yet, so return
  // TRUE so that this rule is run for this identifier.
  else {
    $answer = TRUE;
  }

  return $answer;
}

/**
 * Record that a rule with a particular identifer has run.
 *
 * @param string $identifier
 *   A string used for identifying this rule.
 */
function rules_run_once_record_run($identifier) {
  // Record that the rule ran.
  db_merge('rules_run_once')
    ->key(array('identifier' => $identifier))
    ->fields(array('date' => time()))
    ->execute();
}
